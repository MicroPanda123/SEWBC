{
  description = "SEWBC backend";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    systems.url = "github:nix-systems/default";
    crane.url = "github:ipetkov/crane";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rust-analyzer-src.follows = "";
    };
  };

  outputs = { self, nixpkgs, systems, crane, fenix, ... }: let
    forEachSystem = nixpkgs.lib.genAttrs (import systems);
    pkgsForEach = nixpkgs.legacyPackages;
  in {
    packages = forEachSystem (
      system: let
        pkgs = pkgsForEach.${system};
        craneLib = (crane.mkLib pkgs).overrideToolchain (
          fenix.packages.${system}.complete.withComponents [
            "cargo"
            "rustc"
            "rust-src"
          ]
        );
      in {
        default = craneLib.buildPackage {
          pname = "sewbc-ws";
          version = "0.1.0";
          src = craneLib.cleanCargoSource ./.;
        };
      }
    );

    devShells = forEachSystem (
      system: let
        pkgs = pkgsForEach.${system};
      in {
        default = pkgs.mkShell {
          name = "sewbc-ws";

          packages = with pkgs; [
            rust-analyzer
          ];
          
          inputsFrom = [ self.packages.${system}.default ];
        };
      }
    );
  };
}

