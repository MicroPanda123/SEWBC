use futures_util::{future, pin_mut, stream::StreamExt, SinkExt};
use serde_json::Value;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use tokio::net::TcpListener;
use tokio::sync::mpsc::{unbounded_channel, UnboundedSender};
use tokio_tungstenite::{accept_async, tungstenite::protocol::Message};

#[derive(Clone)]
struct Client {
    id: usize,
    sender: UnboundedSender<Message>,
    room_id: String,
}

type Clients = Arc<Mutex<HashMap<usize, Client>>>;

#[tokio::main]
async fn main() {
    println!("Serving at localhost:6789");
    let addr = "127.0.0.1:6789";
    let clients: Clients = Arc::new(Mutex::new(HashMap::new()));
    let listener = TcpListener::bind(&addr).await.expect("Failed to bind");

    let mut id_counter = 0;

    while let Ok((stream, _)) = listener.accept().await {
        id_counter += 1;
        let client_id = id_counter;
        let clients = clients.clone();

        tokio::spawn(async move {
            let ws_stream = accept_async(stream)
                .await
                .expect("Failed to accept WebSocket connection");

            let (mut ws_sender, mut ws_receiver) = ws_stream.split();

            let (tx, mut rx) = unbounded_channel();

            let new_client = Client {
                id: client_id,
                sender: tx,
                room_id: "General".to_string(),
            };

            clients.lock().unwrap().insert(client_id, new_client.clone());

            send_user_count(&clients).await;

            let clients_for_read = clients.clone();
            let read_future = async {
                while let Some(msg_result) = ws_receiver.next().await {
                    match msg_result {
                        Ok(msg) => {
                            if msg.is_text() {
                                process_message(client_id, msg.into_text().unwrap(), &clients_for_read).await;
                            } else if msg.is_close() {
                                break;
                            }
                        }
                        Err(e) => {
                            eprintln!("Error receiving message: {:?}", e);
                            break;
                        }
                    }
                }
            };

            let write_future = async {
                while let Some(msg) = rx.recv().await {
                    if ws_sender.send(msg).await.is_err() {
                        break;
                    }
                }
            };

            pin_mut!(read_future, write_future);
            future::select(read_future, write_future).await;

            clients.lock().unwrap().remove(&client_id);
            send_user_count(&clients).await;
        });
    }
}

async fn send_user_count(clients: &Clients) {
    let clients_lock = clients.lock().unwrap();
    let user_count = clients_lock.len();
    let message = serde_json::json!({
        "type": "userCount",
        "count": user_count
    })
    .to_string();

    for client in clients_lock.values() {
        let _ = client.sender.send(Message::Text(message.clone()));
    }
}

async fn process_message(client_id: usize, msg_text: String, clients: &Clients) {
    let data: Value = match serde_json::from_str(&msg_text) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Invalid JSON: {}", e);
            return;
        }
    };

    let mut clients_lock = clients.lock().unwrap();

    if let Some(client) = clients_lock.clone().get_mut(&client_id) {
        if data.get("message").is_some() {
            let room_id = data
                .get("roomID")
                .and_then(|v| v.as_str())
                .unwrap_or("General");

            let message = msg_text.clone();

            for other_client in clients_lock.values_mut() {
                if other_client.id != client_id && other_client.room_id == room_id {
                    let _ = other_client.sender.send(Message::Text(message.clone()));
                }
            }
        }

        if let Some(new_room_id_value) = data.get("newRoomID") {
            if let Some(new_room_id) = new_room_id_value.as_str() {
                client.room_id = new_room_id.to_string();
            }
        }
    }
}
