import asyncio
import websockets
import json

clients = []

def edit_client_roomid(ws, roomID):
    for client in clients:
        if client[0] == ws:
            client[1] = roomID
            return True
    return False

def remove_client_by_ws(ws):
    for client in clients:
        if client[0] == ws:
            clients.remove(client)
            return True
    return False

async def send_user_count():
    user_count = len(clients)
    for client in clients:
        await client[0].send(json.dumps({"type": "userCount", "count": user_count}))

async def chat(websocket, path):
    clients.append([websocket, "General"])
    await send_user_count()
    try:
        async for message in websocket:
            data = json.loads(message)
            if "message" in data:
                roomID = data["roomID"]
                for client in clients:
                    if client[0] != websocket and client[1] == roomID:
                        await client[0].send(json.dumps(data))
            if "newRoomID" in data:
                roomID = data["newRoomID"]
                edit_client_roomid(websocket, roomID)
    finally:
        remove_client_by_ws(websocket)
        await send_user_count()

print("Serving at localhost:6789")
start_server = websockets.serve(chat, "localhost", 6789)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
