# Security

[**TLDR**](#tldr)

## Dependencies

This project depends on 2 other: TailWindCSS and HtmlSanitizer. (Links in README.md) 
To make this project independent from CDN's, I downloaded them, and put in the `static` folder.
This makes it run without connecting to CDN's, but also limits ability to update those. (Which I don't find as a huge problem, but it's something you might wanna know)
As far as security goes, HtmlSanitizer is NOT minified, so it was easy to read and it does not seem to have any real problems that would create security risk (except possible false negatives where received HTML is not sanitized properly). 

As for Tailwind, it's minified so who tf knows what is going on in there, but it's used all around the internet and it's mostly style helper, so I don't find it too dangerous, but I am planning to make version that does not use it.

## Server

Server code is pretty simple, because logic there is pretty simple, but there are problems I see.
First of all, it's very basic. Which means, it is prone to being overloaded fast. For small user counts it's fine, and I don't see it being used for big ones, but it is something to look out for. I also believe it's prone for race conditions, I can't explain exactly where I see it, but it's a feeling. Also there is currently 0 ways of authenticating to a server, also something to look out for (Especially when it comes to user "accounts"). 

I am considering rewriting it in Rust later.

## Client

Client code is, I'll say, bad. It works, and I think it's technically fine, but it's ugly, dirty, full of stuff that does not fit, and is generally bad. But I think it does not impact security. After some refactoring it might be even fine. 

## Encryption

Messages are encrypted using AES-GCM, which is industry standard (but old, maybe will switch to something never some time), for that I'm using built in JavaScript crypto. IV and Salt are generated randomly for every message, and key is derived using PBKDF2 from user provided encryption passphrases. Both message and username are encrypted using the same key, but both username and message are (currently) treated as separate messages, so for each username and message a different IV and Salt are generated. Upon receiving a message, it's first tries decrypting using user provided key, for now upon failure (for example wrong key), message is just not decrypted, and appears to be not received (althrough for debug purposes currently message data is logged in console). When decryption succeedes, it's being HTML sanitized, and displayed, alongside username (which goes through the same process). 

I see encrypting username that way kinda weird, but it was pretty simple to do lol, and I don't feel like changing it.

Problematic thing is that currently there is no way to verify a person on the other side, as anyone can have any username they want, which is a result of E2EE nature of this project. While I can see implications of this, if we assume that 2 or more parties decided upon encryption key and roomID in other, secure way, I don't see this as a huge problem.

One of the issues here, is that if adversary is on the same room, but they don't have the encryption key, they still can read encrypted messages, as client still receives them, which allows for bruteforcing weak passphrases or SNDL (Store now, Decrypt later) attacks. This however would require being in the same room as clients.

## Other

Messages are not saved anywhere. Server just takes them, and broadcasts them to clients that are in the same room. I don't know if I will ever change it, for now I don't have plans for that. 

One risk you can assume, is that project shows current user count, which in some threat models could be problematic. But let's be honest, if you truly worry about your threat model, you should use standard tools like Signal or SimpleX, not random project made by teenager (I would be flattered, but I'd recommend you think twice about such decision).

## TLDR
- This project uses dependencies that, while seeming secure, could add potential risk.
- Code of this project is pretty bad, but does not seem to impact security.
- Encryption keys are derived from user provided passphrases.
- Encryption in this project is based on AES-GCM and PBKDF2, both industry standards which are pretty old and might be replaced later (but do not have risk on their own).
- This project displays current user count, but does not disclose more information about them.
- This project does not save messages.
- This project does not contain any user verification.
- Adversary can still read encrypted form of messages that were sent in the same room. This creates bruteforce of SNDL (Store now, Decrypt later) risk.
- If your threat model is serious enough, do not use this project, use something standard like Signal or SimpleX (or even PGP).