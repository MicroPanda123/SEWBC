# Simple End-to-End Encrypted Web-Based Chat

This is a very simple proof of concept end-to-end encrypted chat web app that supports three main functionalities: end-to-end encryption, sending messages, and displaying the number of connected users.

The current implementation consists of a simple WebSocket RUST server and a single HTML (+ 2 dependencies) file for the website. The WebSocket server acts as a message relay, with the majority of the functionality happening in the browser.

Code is ugly. It's a huge mess, but it works. I also can not promise that it's secure. I am pleased with it, but I am not a security expert.

## Backend
Backend has been rewritten in rust :3

Idk it's performance but it is now in rust.

## Dependencies (Big thanksies)
- [HtmlSanitizer by jitbit](https://github.com/jitbit/HtmlSanitizer)
- [TailwindCSS](https://tailwindcss.com/)

## Removal of HTMX
It was discovered that HTMX was unnecessary for this project and has been removed. lol

## Implemented
As for now, I've implemented:

- PAKE-like End-to-end encryption
- Sending messages
- Displaying the number of connected users
- Usernames (poor, barebones, but it works)
- HTML sanitization

## TBD
Better security I suppose.
